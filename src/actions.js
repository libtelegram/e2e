const { Message, InlineQuery } = require('@libtelegram/telegram/types');

exports.send = function (text, extra, done) {
	if (!done) {
		done = extra;
	}

	const msg = new Message({ 
		message_id: 1234567890, 
		text, 
		...extra 
	}, this.bot);
	done();

	this.bot.handle(this.user, msg);	
}

exports.photo = function (photo, caption, extra, done) {
	if (!done) {
		done = extra;
	}

	const msg = new Message({ 
		message_id: 1234567890, 
		photo, caption, 
		...extra 
	}, this.bot);
	done();

	this.bot.handle(this.user, msg);	
}

exports.inline = function (query, done) {
	const msg = new InlineQuery({ id: 1234567890, query }, this.bot);
	done();

	this.bot.handle(this.user, msg);	
}


exports.wait = function (arg, done) {
	if (arguments.length === 1) {
		return setTimeout(arg, 1);
	}

	if (typeof arg === 'function') {
		function after (msg) {
			done(null, arg(msg));
	
			clearTimeout(timer);
		}
	
		const timer = setTimeout(() => {
			this.bot.emitter.removeListener('message', after);
	
			done('Time out');
		}, 1500)
	
		this.bot.emitter.once('message', after);
	} else {
		if (typeof arg === 'number') {
			setTimeout(done, arg);
		}
	}
}