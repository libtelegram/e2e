const initAPI = require('./api');

const user = require('./user');

function e2e (bot) {
	initAPI(bot, this);

	return user(bot, this);
}

module.exports = e2e;