const actions = require('./actions'),
      { User: TelegramUser } = require('@libtelegram/telegram/types');

function User (bot, options = {}) {
	this.bot = bot;

	options.Promise = options.Promise || Promise;

	this.options = options;
	this._queue = [];

	this.user = new TelegramUser(Object.assign({id: 213123 }, options), bot)
}

User.prototype.queue = function () {
	if (!arguments.length) return this._queue
  var args = sliced(arguments)
  var fn = args.pop()
  this._queue.push([fn, args])
}

/**
 * run
 */

User.prototype.run = function(fn) {
  const steps = this.queue();
  this.running = true;
  this._queue = [];
	const self = this;

  // kick us off
  next();

  // next function
  function next(err, _res) {
		const item = steps.shift();

		// Immediately halt execution if an error has been thrown, or we have no more queued up steps.
    if (err || !item) return done.apply(self, arguments);
    const args = item[1] || [];
		const method = item[0];
		args.push(after);

		method.apply(self, args);
	}
	
	function after (err, _res) {
		next.apply(self, arguments);
	}

  function done() {
    const doneargs = arguments;
    self.running = false;

    return fn.apply(self, doneargs);
  }

  return this;
}

User.prototype.then = function (fulfill, reject) {
	return new this.options.Promise((success, failure) => {
    this.run(function(err, result) {
      if (err) failure(err)
      else success(result)
    })
  }).then(fulfill, reject);
}


// wrap all the functions in the queueing function
function queued(name, fn) {
  return function action() {
		var args = [].slice.call(arguments)
    this._queue.push([fn, args])
    return this;
  }
}

/**
 * Attach all the actions.
 */

Object.keys(actions).forEach(function(name) {
  User.prototype[name] = queued(name, actions[name]);
})


module.exports = function (bot) {
	return function (opts) {
		return new User(bot, opts);
	}
}