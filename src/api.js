const EventEmitter = require('events');
const { Message } = require('@libtelegram/telegram/types');

function initAPI (bot) {
	bot.emitter = new EventEmitter();

	bot.sendMessage = function (message_id, text, extra) {
		bot.emitter.emit('message', new Message({
			message_id, text, ...extra
		}));
	}
}

module.exports = initAPI;