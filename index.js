/*!
 * LibTelegram-e2e - a library for end-to-end testing telegram bots
 * telegram bots written in LibTelegram
 * 
 * Made with <3 by @martiliones
 */

'use strict';

module.exports = require('./src/index');